use std::ffi::OsString;
use std::net::{IpAddr, Ipv6Addr};

#[inline]
pub fn osstring_starts_with(data: OsString, prefix: char) -> bool {
    match data.into_string() {
        Ok(s) => matches!(s.chars().next(), Some(c) if c == prefix),
        Err(_) => false,
    }
}

pub fn get_port() -> u16 {
    match std::env::var("PORT").ok() {
        Some(s) => s.parse().unwrap(),
        None => 3000,
    }
}

pub fn get_bind_host() -> IpAddr {
    match std::env::var("BIND").ok() {
        Some(s) => s.parse().unwrap(),
        None => IpAddr::V6(Ipv6Addr::UNSPECIFIED),
    }
}
