use async_std::fs::read_to_string;
use async_std::path::PathBuf;
use serde::Serialize;
use std::collections::HashSet;

#[derive(Debug, Serialize)]
pub struct DokkuApp {
    name: String,
    hosts: HashSet<String>,
}

impl DokkuApp {
    pub async fn try_from_path(path: PathBuf) -> Option<Self> {
        let vhost_path = path.join("VHOST");

        if !vhost_path.is_file().await {
            return None;
        }

        let current_vhosts = read_to_string(&vhost_path).await.ok()?;

        if current_vhosts.is_empty() {
            return None;
        }

        Some(DokkuApp {
            name: String::from(path.file_name()?.to_str()?),
            hosts: current_vhosts.lines().map(String::from).collect(),
        })
    }
}

pub fn get_dokku_root() -> Option<std::path::PathBuf> {
    // First, check if we have a custom directory
    if let Some(dokku_root) = std::env::var_os("DOKKU_ROOT") {
        return Some(std::path::PathBuf::from(dokku_root));
    }

    // Next, check for a dokku user and its home directory
    match nix::unistd::User::from_name("dokku") {
        Ok(Some(user)) => return Some(user.dir),

        // If the user doesn't exist, do nothing
        Ok(None) => return None,

        // If there was an error, give up
        _ => {}
    }

    // Finally, try a hard-coded path
    let dokku_home_guess = std::path::PathBuf::from("/home/dokku");
    if dokku_home_guess.is_dir() {
        return Some(dokku_home_guess);
    }

    // If there's nothing else, give up
    None
}
